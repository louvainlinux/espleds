# Louvain-li-Nux LEDs

## Description

This project utilizes Arduino and an ESP8266 to control LED strip lights through a web interface. It allows users to adjust brightness and select different lighting effects for the LED strip. 

## Features

- Adjust the brightness of the LED strip.
- Select from a variety of lighting effects.
- Web-based control interface.

## Installation

1. Clone the repository: git clone https://gitlab.com/louvainlinux/espleds
2. Install the required libraries:
    - ESPAsyncWebServer
    - Adafruit NeoPixel
    - NTPClient
3. Connect the LED strip to the appropriate pins on the Arduino board. By default, data is on Pin D1.
4. Copy the example configuration file and edit it with the right values and eventually change the other configurations, such as IP addresses, GPIO pin, ...
5. Upload the sketch and the filesystem to your Arduino board.

## Usage

1. Ensure the Arduino board is connected to the LED strip and powered on.
2. Open a web browser and navigate to the IP address of the Arduino board.
3. The web interface will be displayed, allowing you to control the LED strip.
4. Adjust the brightness using the provided box.
5. Select your desired lighting effect from the options provided.
6. Enjoy the dynamic lighting effects on your LED strip!

## License

This project is licensed under the [MIT License](LICENSE).