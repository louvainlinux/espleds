#ifndef OTA_H
#define OTA_H 

#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

#include <leds.h>


bool otaStart() {
  return ArduinoOTA.getCommand() != U_FLASH;
}

void otaEnd() {
  setup();
}

void otaProgress(unsigned int progress, unsigned int total) {
    int on = float(progress)/float(total) * LED_CNT;
    for (int i = 0; i < LED_CNT; i++) {
        if(i < on) strip.setPixelColor(i, getColor(255, 30, 0));
        else strip.setPixelColor(i, COLOR_BLACK);
    }
    strip.show();
    
}

void otaError(ota_error_t error) {
    for (int i = 0; i < 25; i++) {
        strip_fill(COLOR_RED);
        delay(100);
        strip_fill(COLOR_BLACK);
        delay(100);
    }
}

void setupOTA(const char *hostname) {
  ArduinoOTA.setHostname(hostname);

  ArduinoOTA.onStart(otaStart);
  ArduinoOTA.onEnd(otaEnd);
  ArduinoOTA.onProgress(otaProgress);
  ArduinoOTA.onError(otaError);
}

void setupOTA(const char *hostname, const char *password) {
  ArduinoOTA.setPassword(password);
  setupOTA(hostname);
}

void setupOTA(const char *hostname, const int port) {
  ArduinoOTA.setPort(port);
  setupOTA(hostname);
}

void setupOTA(const char *hostname, const char *password, const int port) {
  ArduinoOTA.setPassword(password);
  ArduinoOTA.setPort(port);
  setupOTA(hostname);
}

void beginOTA() {
    ArduinoOTA.begin();
}

#endif