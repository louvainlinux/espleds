#ifndef EFFECTS_H
#define EFFECTS_H

#include <EEPROM.h>

#include <leds.h>
#include <wifi.h>

#include "effects/constant_color.h"
#include "effects/epileptic.h"
#include "effects/rainbow.h"
#include "effects/laser.h"
#include "effects/double_laser.h"
#include "effects/sparkle.h"
#include "effects/thunderstorm.h"
#include "effects/epileptic_white.h"
#include "effects/timer.h"
#include "effects/timestamp.h"
#include "effects/music_sync.h"
#include "effects/linear_clock.h"
#include "effects/split_clock.h"
#include "effects/binary_clock.h"
#include "effects/twinkle.h"
#include "effects/running_light.h"
#include "effects/pause.h"

struct Effect {
  uint8_t id;
  void (*function)(int);
  int arg;
  String title;

  void run() {
    if(function != nullptr) function(arg);
  }
};

void setStripEffect(uint8_t id){
  currentEffectId = id;
  EEPROM.put(1, id);
  EEPROM.commit();
}

Effect efs[] = {
  {0, &constant_color, COLOR_BLACK, "Off"},
  {2, &constant_color, COLOR_WHITE, "White"},
  {19, &pause, 100, "Pause"},
  {1, &constant_color, COLOR_PINK, "Pink"},
  {3, &epileptic, 200, "Epileptic"},
  {4, &rainbow, 10, "Rainbow"},
  {5, &kitt, 10, "Laser"},
  {6, &doubleKitt, 5, "Double lasers"},
  {7, &multiColorSparkle, 50, "Sparkles"},
  {8, &thunderstorm, 1000, "Thunderstorm"},
  {9, &epilepticWhite, 50, "Fast epileptic"},
  {10, &epilepticWhite, 500, "Slow epileptic"},
  {11, &timer, 100, "Timer"},
  {12, &timestamp, 100, "Timestamp"},
  {13, &syncWithMusic, 10, "Sync with music"},
  {14, &altClock, 10, "Linear clock"},
  {15, &clock3, 1000, "Split clock"},
  {16, &clockBin, 1000, "Binary clok"},
  {17, &twinkle, LED_CNT / 4, "Twinkle"},
  {18, &runningLight, 50, "Running light"},
};

int n_effect = sizeof(efs)/sizeof(Effect);

void loadStripEffect(){
  lastApplied = currentEffectId;
  
  Effect effect = efs[currentEffectId];

  effect.run();
}

String createMenu() {
  String options;
  for (int i = 0; i < n_effect; i++){
    options += "<option value=\"";
    options += String(i);
    options += "\" ";
    if(i == currentEffectId){
      options += "selected=\"selected\"";
    }
    options += ">";
    options += efs[i].title;
    options += "</option>";
  }

  return options;
}

#endif