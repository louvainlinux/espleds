#ifndef EFFECT_PAUSE_H
#define EFFECT_PAUSE_H

#include "leds.h"

void pause(int wait) {
  delay(wait);
}

#endif