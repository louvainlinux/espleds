#ifndef EFFECT_EPILEPTIC_WHITE_H
#define EFFECT_EPILETIC_WHITE_H

#include "leds.h"

void epilepticWhite(int wait) {
  strip_fill(COLOR_WHITE);
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
  strip_fill(COLOR_BLACK);
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif