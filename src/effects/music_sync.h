#ifndef EFFECT_MUSIC_SYNC_H
#define EFFECT_MUSIC_SYNC_H

#include "leds.h"
#include "wifi.h"

void syncWithMusic(int wait) {
  int packetSize = musicUDP.parsePacket();

  if(packetSize) {
    int len = musicUDP.read(musicPacketBuffer, BUFFER_LEN);
    for (size_t i = 0; i < len; i+=4){
      musicPacketBuffer[len] = 0;
      uint8_t n = musicPacketBuffer[i];
      uint32_t color = strip.Color((uint8_t)musicPacketBuffer[i+1], (uint8_t)musicPacketBuffer[i+2], (uint8_t)musicPacketBuffer[i+3]);
      strip.setPixelColor(n, color);
    }
    strip.show();
  }
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif