#ifndef EFFECT_BINARY_CLOCK_H
#define EFFECT_BINARY_CLOCK_H

#include "leds.h"
#include "wifi.h"

void clockBin(int wait) {
  strip.clear();
  int i = 0;
  strip.setPixelColor(i=0, COLOR_BLUE);
  for (size_t j = 0; j < 4; j++){
    if((timeClient.getDay() >> j & 1) == 1) strip.setPixelColor(i+=1, COLOR_GREEN);
    else strip.setPixelColor(i+=1, COLOR_BLACK);
  }
  strip.setPixelColor(i+=1, COLOR_BLUE);
  for (size_t j = 0; j < 5; j++){
    if(((timeClient.getHours()+2) >> j & 1) == 1) strip.setPixelColor(i+=1, COLOR_GREEN);
    else strip.setPixelColor(i+=1, COLOR_BLACK);
  }
  strip.setPixelColor(i+=1, COLOR_BLUE);
  for (size_t j = 0; j < 6; j++){
    if((timeClient.getMinutes() >> j & 1) == 1) strip.setPixelColor(i+=1, COLOR_GREEN);
    else strip.setPixelColor(i+=1, COLOR_BLACK);
  }
  strip.setPixelColor(i+=1, COLOR_BLUE);
  for (size_t j = 0; j < 6; j++){
    if((timeClient.getSeconds() >> j & 1) == 1) strip.setPixelColor(i+=1, COLOR_GREEN);
    else strip.setPixelColor(i+=1, COLOR_BLACK);
  }
  strip.setPixelColor(i+=1, COLOR_BLUE);
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif