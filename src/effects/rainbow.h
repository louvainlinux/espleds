#ifndef EFFECT_RAINBOW_H
#define EFFECT_RAINBOW_H

#include "leds.h"

void rainbow(int wait) {
  for (long firstPixelHue = 0; firstPixelHue < 5 * 65536; firstPixelHue += 256) {
    for (int i = 0; i < LED_CNT; i++) {
      int pixelHue = firstPixelHue + (i * 65536 / LED_CNT);
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(wait);
  }
}

#endif