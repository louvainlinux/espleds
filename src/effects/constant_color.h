#ifndef EFFECT_CONSTANT_COLOR_H
#define EFFECT_CONTANT_COLOR_H

#include "leds.h"

void constant_color(int color) {
  strip_fill(color);
  strip.show();
  delay(100);
}

#endif