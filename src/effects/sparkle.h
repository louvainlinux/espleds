#ifndef EFFECT_SPARKLE_H
#define EFFECT_SPARKLE_H

#include "leds.h"

void multiColorSparkle(int wait) {
  for (int i = 0; i < LED_CNT/6; i++) {
    strip.setPixelColor(random(0, LED_CNT), COLOR_WHITE);
  }
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
  strip.clear();
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif