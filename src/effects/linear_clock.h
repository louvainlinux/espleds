#ifndef EFFECT_LINEAR_CLOCK_H
#define EFFECT_LINEAR_CLOCK_H

#include "leds.h"
#include "wifi.h"

void altClock(int wait) {
  for (int i = 0; i < 60; i++){
    if(i+1 == timeClient.getSeconds()) {
      strip.setPixelColor(i, COLOR_BLUE);
    } else if(i+1 == timeClient.getHours()) {
      strip.setPixelColor(i, COLOR_RED);
    } else if(i+1 == timeClient.getMinutes()) {
      strip.setPixelColor(i, COLOR_GREEN);
    } else if(i+1 <= timeClient.getHours()) {
      strip.setPixelColor(i, COLOR_RED);
    } else if(i+1 <= timeClient.getMinutes()) {
      strip.setPixelColor(i, COLOR_GREEN);
    } else {
      strip.setPixelColor(i, COLOR_BLACK);
    }
  }

  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif