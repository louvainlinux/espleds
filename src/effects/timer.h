#ifndef EFFECT_TIMER_H
#define EFFECT_TIMER_H

#include "leds.h"

void timer(int wait) {
  uint64_t i = 0;
  strip.clear();
  while (true){
    for (size_t j = 0; j < LED_CNT; j++){
      if((i >> j & 1) == 1) strip.setPixelColor(j, COLOR_BLUE);
      else {
        strip.setPixelColor(j, COLOR_BLACK);
        if(i >> j == 0) break;
      }
    }
    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(wait);
    i++;
  }
}

#endif