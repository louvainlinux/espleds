#ifndef EFFECT_TWINKLE_H
#define EFFECT_TWINKLE_H

#include "leds.h"

void twinkle(int wait) {
  uint32_t color = random(0 , 2) == 0 ? COLOR_BLACK : strip.Color(random(0, 255), random(0, 255), random(0, 255));
  strip.setPixelColor(random(0, LED_CNT), color);
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif