#ifndef EFFECT_LASER_H
#define EFFECT_LASER_H

#include "leds.h"

void kitt(int wait) {
  int red = 20;
  int green = 150;
  int blue = 200;
  int fade = 10;
  uint32_t color = strip.Color(red, green, blue);

  strip.clear();
  for (int i = 0; i < LED_CNT - fade/2; i++) {
    for (float j = 0; j < fade; j++) {
      float f = (j/fade)*(j/fade);
      strip.setPixelColor(i + j, red * f, green * f, blue * f);
    }

    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(wait);
  }

  for (int i = LED_CNT; i > fade/2; i--) {
    for (float j = 0; j < fade; j++) {
      float f = (j/fade)*(j/fade);
      strip.setPixelColor(i - j, red * f, green * f, blue * f);
    }

    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(wait);
  }
}

#endif