#ifndef EFFECT_RUNNING_LIGHT_H
#define EFFECT_RUNNING_LIGHT_H

#include "leds.h"

void runningLight(int wait) {
  int red = 200;
  int green = 0;
  int blue = 100;
  int pos = 0;

  for (int i = 0; i < LED_CNT * 2; i++){
    pos++;
    for (int j = 0; j < LED_CNT; j++) {
      strip.setPixelColor(j, ((sin(j + pos) * 127 + 128) / 255) * red,
                          ((sin(j + pos) * 127 + 128) / 255) * green,
                          ((sin(j + pos) * 127 + 128) / 255) * blue);
    }
    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(wait);
  }
}

#endif