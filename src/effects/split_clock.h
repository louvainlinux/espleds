#ifndef EFFECT_SPLIT_CLOCK_H
#define EFFECT_SPLIT_CLOCK_H

#include "leds.h"
#include "wifi.h"

void clock3(int wait){
  strip.clear();
  int i = 0;
  strip.setPixelColor(i, timeClient.getHours()%24 > 12 ? COLOR_GREEN : COLOR_BLACK);
  strip.setPixelColor(i+=1, COLOR_BLUE);
  for (size_t j = 0; j < 12; j++){
    if((timeClient.getHours()-1) % 12 >= j) strip.setPixelColor(i+=1, COLOR_GREEN);
    else strip.setPixelColor(i+=1, COLOR_BLACK);
  }
  strip.setPixelColor(i+=1, COLOR_BLUE);
  for (size_t j = 0; j < 30; j++){
    if(timeClient.getMinutes()/2 > j) strip.setPixelColor(i+=1, COLOR_GREEN);
    else strip.setPixelColor(i+=1, COLOR_BLACK);
  }
  strip.setPixelColor(i+=1, COLOR_BLUE);
  for (size_t j = 0; j < 12; j++){
    if(timeClient.getSeconds()/5 > j) strip.setPixelColor(i+=1, COLOR_GREEN);
    else strip.setPixelColor(i+=1, COLOR_BLACK);
  }
  strip.setPixelColor(i+=1, COLOR_BLUE);
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif