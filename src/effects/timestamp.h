#ifndef EFFECT_TIMESTAMP_H
#define EFFECT_TIMESTAMP_H

#include "leds.h"
#include "wifi.h"

void timestamp(int wait) {
  uint64_t i = timeClient.getEpochTime();
  strip.clear();
  for (size_t j = 0; j < LED_CNT; j++){
    if((i >> j & 1) == 1) strip.setPixelColor(j, COLOR_RED);
    else {
      strip.setPixelColor(j, COLOR_BLACK);
      if(i >> j == 0) break;
    }
  }
  strip.show();
  if(lastApplied != currentEffectId) return;
  delay(wait);
}

#endif