#ifndef EFFECT_THUNDERSTORM_H
#define EFFECT_THUNDERSTORM_H

#include "leds.h"

void thunderstorm(int wait) {
  //Number of flashes for each loop
  int flashCnt = random(3, 15);

  //Brightness range for each individuell flash
  int minBrightness = 10;
  int maxBrightness = 255;

  //Delay between each flash in one loop
  int minDelay = 1;
  int maxDelay = 150;

  int loopDelay = random(wait, wait*5);  //Delay after one loop

  for (int flash = 0; flash <= flashCnt; flash++) {
    int brightness = random(minBrightness, maxBrightness);
    strip.fill(strip.Color(brightness, brightness, brightness), 0, LED_CNT);
    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(random(minDelay, maxDelay));
    strip.clear();
    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(random(minDelay, maxDelay));
  }
  if(lastApplied != currentEffectId) return;
  delay(loopDelay);
}

#endif