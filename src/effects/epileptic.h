#ifndef EFFECT_EPILEPTIC_H
#define EFFECT_EPILEPTIC_H

#include "leds.h"

void epileptic(int wait) {
  unsigned int colors[] = {
    strip.Color(0, 0, 0),
    strip.Color(255, 0, 0),
    strip.Color(0, 255, 0),
    strip.Color(0, 0, 255),
    strip.Color(255, 255, 0),
    strip.Color(0, 255, 255),
    strip.Color(255, 0, 255),
    strip.Color(255, 255, 255),
  };

  for (size_t j = 0; j < 8; j++) {
    unsigned int color = colors[j];
    strip_fill(color);
    strip.show();   
    if(lastApplied != currentEffectId) return;
    delay(wait); 
  }
}

#endif