#ifndef EFFECT_DOUBLE_LASER_H
#define EFFECT_DOUBLE_LASER_H

#include "leds.h"

void doubleKitt(int wait) {
  int red = 200;
  int green = 0;
  int blue = 120;
  int wide = 5;
  int j = LED_CNT;
  for (int i = 0; i < (LED_CNT / 2) - wide -2; i++) {
    j = LED_CNT - i;

    strip.setPixelColor(i - 2, red / 4, green / 4, blue / 4);
    strip.setPixelColor(i - 1, red / 2, green / 2, blue / 2);
    strip.setPixelColor(i + 1 + wide, red / 4, green / 4, blue / 4);
    strip.setPixelColor(i + wide, red / 2, green / 2, blue / 2);
    strip.setPixelColor(j - 2, red / 4, green / 4, blue / 4);
    strip.setPixelColor(j - 1, red / 2, green / 2, blue / 2);
    strip.setPixelColor(j + 1 + wide, red / 4, green / 4, blue / 4);
    strip.setPixelColor(j + wide, red / 2, green / 2, blue / 2);

    for (int k = 0; k < wide; k++) {
      strip.setPixelColor(i + k, red, green, blue);
      strip.setPixelColor(j + k, red, green, blue);
    }

    strip.show();

    if(lastApplied != currentEffectId) return;
    delay(wait);
    strip.clear();
    if(lastApplied != currentEffectId) return;
    delay(wait);
    strip.show();
  }
  for (int i = LED_CNT / 2; i > 0 + wide + 2; i--) {
    j = LED_CNT - i;

    strip.setPixelColor(j - 2, red / 4, green / 4, blue / 4);
    strip.setPixelColor(j - 1, red / 2, green / 2, blue / 2);
    strip.setPixelColor(j + 1 + wide, red / 4, green / 4, blue / 4);
    strip.setPixelColor(j + wide, red / 2, green / 2, blue / 2);
    strip.setPixelColor(i - 2, red / 4, green / 4, blue / 4);
    strip.setPixelColor(i - 1, red / 2, green / 2, blue / 2);
    strip.setPixelColor(i + 1 + wide, red / 4, green / 4, blue / 4);
    strip.setPixelColor(i + wide, red / 2, green / 2, blue / 2);

    for (int k = 0; k < wide; k++) {
      strip.setPixelColor(i + k, red, green, blue);
      strip.setPixelColor(j + k, red, green, blue);
    }

    strip.show();
    if(lastApplied != currentEffectId) return;
    delay(wait);
    strip.clear();
    if(lastApplied != currentEffectId) return;
    delay(wait);
    strip.show();
  }
}

#endif