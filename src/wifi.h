#ifndef WIFI_H
#define WIFI_H

#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include <leds.h>
#include <config.h>

const IPAddress localIp(10, 0, 0, 100);
const IPAddress gateway(10, 0, 0, 1);
const IPAddress subnet(255, 255, 254, 0);
const IPAddress primaryDNS(10, 0, 0, 1);
const IPAddress secondaryDNS(1, 1, 1, 1);

#define BUFFER_LEN 1024
unsigned int musicPort = 7777;
char musicPacketBuffer[BUFFER_LEN];
WiFiUDP musicUDP;

#define NTP_OFFSET   60 * 60 * 2       // In seconds
#define NTP_INTERVAL 60 * 60 * 1000    // In miliseconds
#define NTP_ADDRESS  "europe.pool.ntp.org"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

int wlanStatus() {
  switch (WiFi.status()) {
  case WL_IDLE_STATUS:
    return 0;
  case WL_SCAN_COMPLETED:
    return 1;
  case WL_CONNECTED:
    Serial.print("WLAN connected ");
    Serial.println(WiFi.localIP().toString());
    return 2;
  case WL_NO_SSID_AVAIL:
    return -1;
  case WL_CONNECT_FAILED:
    return -2;
  case WL_CONNECTION_LOST:
    return -3;
  case WL_WRONG_PASSWORD:
    return -4;
  case WL_DISCONNECTED:
    return -5;
  default:
    return -7;
  }
}

bool wifi_setup() {
  strip.clear();
  strip.setPixelColor(0, strip.Color(0, 0, 255));
  strip.show();

  WiFi.mode(WIFI_STA);
  WiFi.config(localIp, gateway, subnet, primaryDNS, secondaryDNS);
  WiFi.begin(ssid, password);
  Serial.print("WiFi connecting ");
  for(uint i = 0; (WiFi.status() == WL_IDLE_STATUS || WiFi.status() == WL_DISCONNECTED) && i <= 600;i++) {
    if(i%LED_CNT == 0) strip.clear();
    strip.setPixelColor(i%LED_CNT, strip.Color(0, 0, 255));
    strip.show();
    delay(100);
  }
  Serial.println();
  
  if(wlanStatus() > 0) {
    WiFi.setAutoReconnect(true);
    WiFi.persistent(true);
    timeClient.begin();
    musicUDP.begin(musicPort);

    for (size_t i = 0; i < 2; i++){
      strip_fill(COLOR_GREEN);
      strip.show();
      delay(500);
      strip_fill(COLOR_BLACK);
      strip.show();
      delay(500);
    }


    return true;
  } else {
    for (size_t i = 0; i < 5; i++){
      strip_fill(COLOR_RED);
      strip.show();
      delay(500);
      strip_fill(COLOR_BLACK);
      strip.show();
      delay(500);
    }

    return false;
  }

}

void check_wifi() {
  if(WiFi.status() == WL_CONNECTED) return;
  while(!wifi_setup()) {
    delay(5000);
  }
}

#endif