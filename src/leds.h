#ifndef LEDS_H
#define LEDS_H

#include <Adafruit_NeoPixel.h>

uint8_t currentEffectId = 1;
uint8_t lastApplied = 1;

int LED_CNT = 60;

uint32_t getColor(uint8_t r, uint8_t g, uint8_t b) {
    return ((uint32_t)r << 16) | ((uint32_t)g << 8) | b;
}

Adafruit_NeoPixel strip(LED_CNT, 5, NEO_GRB + NEO_KHZ800);

uint32_t COLOR_BLACK = getColor(0, 0, 0);
uint32_t COLOR_RED = getColor(255, 0, 0);
uint32_t COLOR_GREEN = getColor(0, 255, 0);
uint32_t COLOR_BLUE = getColor(0, 0, 255);
uint32_t COLOR_WHITE = getColor(255, 255, 255);
uint32_t COLOR_PINK = getColor(200, 20, 133);

void strip_fill(uint32_t color) {
    for (size_t i = 0; i < LED_CNT; i++) {
        strip.setPixelColor(i, color);
    }
}

#endif