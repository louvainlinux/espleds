#include <Arduino.h>
#include <wifi.h>
#include <ESPAsyncWebServer.h>
#include <LittleFS.h>
#include <leds.h>
#include <effects.h>
#include <EEPROM.h>
#include <ota.h>


AsyncWebServer server(80);

String processor(const String& var) {
  if(var == "BRIGHTNESS") return String(strip.getBrightness());
  if(var.startsWith("EFFECT_MENU")) {
    return createMenu();
  }
  return "error";
}


void setup() {
  Serial.begin(115200);

  setupOTA("10.0.0.100");
  beginOTA();

  EEPROM.begin(32);
  strip.begin();

  strip.clear();
  uint8_t brightness = 255;
  EEPROM.get(0, brightness);
  strip.setBrightness(brightness);
  strip.show(); 

  if(!LittleFS.begin()) {
    Serial.println("An error has occured while mounting SPIFFS");
    strip_fill(COLOR_RED);
    strip.show();
    return;
  }

  EEPROM.get(1, currentEffectId);

  check_wifi();

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/index.html", String(), false, processor);
  });

  server.on("/index.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/index.css","text/css");
  });

  server.on("/api/brightness", HTTP_GET, [](AsyncWebServerRequest *request){
    if(request->hasParam("value")){
      uint8_t brightness = request->getParam("value")->value().toInt();
      strip.setBrightness(brightness);
      EEPROM.put(0, brightness);
      EEPROM.commit();
      request->send(200, "text/plain", "OK");
    } else {
      request->send(400, "text/plain", "Missing \"value\" parameter");
    }
  });

  server.on("/api/effect", HTTP_GET, [](AsyncWebServerRequest *request){
    if(request->hasParam("id")){
      int effectId = request->getParam("id")->value().toInt();
      setStripEffect(effectId);
      request->send(200, "text/plain", "OK");
    } else {
      request->send(400, "text/plain", "Missing \"value\" parameter");
    }
  });

  server.begin();
}

void loop() {
  check_wifi();
  ArduinoOTA.handle();
  timeClient.update();
  loadStripEffect();
}
